Django==1.6.6
argparse==1.2.1
django-bootstrap-form==3.1
oauthlib==0.6.3
requests==2.3.0
requests-oauthlib==0.4.1
wsgiref==0.1.2
